const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const parent = document.getElementById('id');

function displayList(array, parent = document.body){
    const ul = document.createElement('ul');
    const content = array.map((element) => {
        return `<li>${element}</li>`
    }).join("");
    ul.innerHTML = content;
    parent.prepend(ul);
}

displayList(array);
displayList(array, parent);
